#include <stdio.h>
#include <stdlib.h>

int main()
{
    int stopien, lista[] = {}, temp, wynik;
    printf("Podaj stopien wielomianu: ");
    scanf("%d", &stopien);
    for(int j = 0; j <= stopien; j++)
    {
        printf("Podaj wspolczynnik: \n");
        scanf("%d", &temp);
        lista[j] = temp;
    }
    printf("x   y\n\n");
    for(int x = 0; x <= stopien; x++)
    {
        wynik = 0;
        for(int i = 0; i <= stopien; i++)
            wynik = wynik * x + lista[i];
        printf("%d  %d\n", x, wynik);
    }
}
