#include <stdio.h>

int main()
{
    int temp=0, max=0, min=999999;
    while(temp >= 0)
    {
        printf("Podaj liczbe: ");
        scanf("%d", &temp);

        if(temp > max)
        {
            max = temp;
        }
        if(temp < min && temp > -1)
        {
             min = temp;
        }
    }
    printf("Maksymalny element to: %d, Minimalny to: %d\n", max, min);
}
