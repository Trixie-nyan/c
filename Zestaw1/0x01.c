#include <stdio.h>
int main(){
    int x;
    printf("Podaj liczbe calkowita: ");
    scanf("%d", &x);
    printf("Jestem \\n\ni jestem przejsciem do nastepnej linii\na twoja liczba to %d.\n", x);
    printf("\nJestem \\t i jestem \ttabulatorem.\n");
    printf("\nJestem \\r i wracam kursor do poczatku wiersza.\n");
    printf("\nJestem \\b i u\bsuwam ups mialem na mysli, ze usuwam pojedynczy znak.\n");
    printf("\nJestem \\a i \apowinienem odtworzyc alarm. *beep*\n");
    printf("\nJestem \\f i moim zadaniem bylo kiedys wyrzucic kartke i zaczac na nowej stronie ale mamy 21 wiek i nikt mnie juz nie potrzebuje.\n");
    printf("\nJestem \\\\ i jestem tzw. backslashem \\ \\ \\.\n");
    printf("\nJestem \\ i jesli zostane postawiony przed innymi komendami typu \\n to wtedy jest zwyklym tekstem.\n");
}
