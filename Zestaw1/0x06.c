#include<stdio.h>
#define TABINC 8

int main()
{
    int liczbatab = 0, pozycja = 1, zdanie;
    while((zdanie = getchar()) != EOF)
        {
        if(zdanie == '\t')
        {
            liczbatab = TABINC - (( pozycja - 1) % TABINC);
            for(int i = 0; i < liczbatab; i++)
            {
                putchar('_');
                ++pozycja;
            } // END for
        } // END if
        else if(zdanie == '\n')
        {
            putchar(zdanie);
            pozycja = 1;
        } // END if
        else
        {
            putchar(zdanie);
            ++pozycja;
        } // END else
    } // END while
} // END main
